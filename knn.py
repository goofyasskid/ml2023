import random
import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import load_iris
from itertools import combinations
import copy
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split


def normalize(xarray, xmin, xmax):
    arr = []
    for i in range(len(xarray)):
        arr.append((xarray[i] - xmin[i])/(xmax[i] - xmin[i]))
    return arr


def dist(point_a, point_b):
    return np.sqrt((point_a[0] - point_b[0]) ** 2 +
                   (point_a[1] - point_b[1]) ** 2 +
                   (point_a[2] - point_b[2]) ** 2 +
                   (point_a[1] - point_b[1]) ** 2)


def find_neighbour(neighbour_count, element, points, targets):
    neighbours = {}
    for i, point in enumerate(points):
        neighbours[i] = [dist(element, point), targets[i]]
    neighbours = sorted(neighbours.items(), key=lambda item: item[1][0])
    return neighbours[:int(neighbour_count)]


def find_target(neighbour_):
    targ = {
        0: 0,
        1: 0,
        2: 0,
    }
    for n in neighbour_:
        targ[n[1][1]] += 1
    targ = sorted(targ.items(), key=lambda item: item[1])
    return targ[-1][0]

def find_k(X_learn_normalized, X_test_normalized, y_learn, y_test):

    # X_train_transformed = []
    # X_test_transformed = []
    #
    # min_ = [min(idx) for idx in zip(*X_train)]
    # max_ = [max(idx) for idx in zip(*X_train)]
    # for i in range(len(X_train)):
    #     X_train_transformed.append(list(normalize(X_train[i], min_, max_)))
    #
    # min_ = [min(idx) for idx in zip(*X_test)]
    # max_ = [max(idx) for idx in zip(*X_test)]
    # for i in range(len(X_test)):
    #     X_test_transformed.append(list(normalize(X_test[i], min_, max_)))

    best_accuracy = 0
    best_k = -1
    for k in range(3, 14):
        curr_model = KNeighborsClassifier(n_neighbors=k)
        curr_model.fit(X_learn_normalized, y_learn)
        preds = curr_model.predict(X_test_normalized)
        acc = accuracy_score(y_test, preds)
        if acc > best_accuracy:
            best_accuracy = acc
            best_k = k
            print(f"{best_accuracy}:{best_k}")
        return best_k


def projection(X, y):
    fig, axs = plt.subplots(2, 3)
    comb = list(combinations((0, 1, 2, 3), 2))
    name = ['sepal length', 'sepal width', 'petal length', 'petal width']
    X = np.array(X)
    for i in range(2):
        for j in range(3):
            axs[i, j].scatter(X[:, comb[i*3+j][0]], X[:, comb[i*3+j][1]], c=y)
            axs[i, j].set_title(f'{name[comb[i*3+j][0]]}, and {name[comb[i*3+j][1]]}')
            axs[i][j].set_xlabel(name[comb[i*3+j][0]])
            axs[i][j].set_ylabel(name[comb[i*3+j][1]])
    plt.show()


if __name__ == '__main__':
    iris_data = load_iris()['data']

    min_ = [min(idx) for idx in zip(*iris_data)]
    max_ = [max(idx) for idx in zip(*iris_data)]

    iris_target = load_iris()['target']

    learn_data = {
        'original': [],
        'normalized': []
    }
    learn_target = []

    test_data = {
        'original': [],
        'normalized': []
    }
    test_target = []

    for i in range(len(iris_data)):
        change_flag = random.randint(0, 4)
        if change_flag == 0:
            test_data['normalized'].append(list(normalize(iris_data[i], min_, max_)))
            test_data['original'].append(list(iris_data[i]))
            test_target.append(iris_target[i])
        else:
            learn_data['normalized'].append(list(normalize(iris_data[i], min_, max_)))
            learn_data['original'].append(list(iris_data[i]))
            learn_target.append(iris_target[i])

    projection(learn_data['original'], learn_target)
    projection(learn_data['normalized'], learn_target)

    #k = round(math.sqrt(len(learn_data['normalized'])))
    # k = find_k(iris_data, iris_target)
    k = find_k(learn_data['normalized'], test_data['normalized'], learn_target, test_target)
    print(k)

    show_data = copy.copy(learn_data['normalized'])
    show_target = copy.copy(learn_target)

    for j in range(int(input("Сколько новых вершин добавить: "))):
        point = []
        for i in range(4):
            point.append(round(random.uniform(min_[i], max_[i]), 2))
        point = normalize(point, min_, max_)
        neighbour_ = find_neighbour(k, point, learn_data['normalized'], learn_target)
        target = find_target(neighbour_)
        print(f"{point} : {target}")
        show_data.append(point)
        show_target.append(target)

    projection(learn_data['normalized'], learn_target)
