import networkx as nx
import matplotlib.pyplot as plt
import random

if __name__ == '__main__':
    G = nx.Graph()
    n = 10
    dots = range(n)
    G.add_nodes_from(dots)
    e = []
    for i in range(n):
        buffer = list(range(i+1, n))
        random.shuffle(buffer)
        for j in range(0, random.randint(0, n-i-1)):
            weight = random.randint(1,20)
            e.append((i,buffer.pop(),weight))
    #G.add_edges_from(e)
    G.add_weighted_edges_from(e)
    weights = nx.get_edge_attributes(G, 'weight')
    pos = nx.spring_layout(G)
    nx.draw(G, pos, with_labels = True)
    nx.draw_networkx_edge_labels(G, pos, nx.get_edge_attributes(G, 'weight'))
    plt.show()

    G = nx.Graph()
    G.add_nodes_from(dots)
    min_edge = (0, 1, 20)

    unused_dots = []
    for points in weights.keys():
        if points[0] not in unused_dots:
            unused_dots.append(points[0])
        if points[1] not in unused_dots:
            unused_dots.append(points[1])

    used_dots = []
    new_e = []
    for points, edge in weights.items():
        if edge < min_edge[2]:
            min_edge = (points[0], points[1], edge)

    unused_dots.remove(min_edge[0])
    unused_dots.remove(min_edge[1])

    used_dots.append(min_edge[0])
    used_dots.append(min_edge[1])

    new_e.append((min_edge))

    while len(unused_dots) != 0:
        min_edge = (0, 1, 20)
        for points, edge in weights.items():
            if (points[0] in used_dots and points[1] in unused_dots) or (points[1] in used_dots and points[0] in unused_dots):
                if edge < min_edge[2]:
                    min_edge = (points[0], points[1], edge)
        if min_edge[0] not in used_dots:
            used_dots.append(min_edge[0])
            unused_dots.remove(min_edge[0])
        if min_edge[1] not in used_dots:
            used_dots.append(min_edge[1])
            unused_dots.remove(min_edge[1])
        new_e.append(min_edge)

    G.add_weighted_edges_from(new_e)
    weights = nx.get_edge_attributes(G, 'weight')
    pos = nx.spring_layout(G)
    nx.draw(G, pos, with_labels=True)
    nx.draw_networkx_edge_labels(G, pos, nx.get_edge_attributes(G, 'weight'))
    plt.show()


    k = 4
    for i in range(k - 1):
        max_edge = (0, 1, 0)
        for edge in new_e:
            if edge[2] > max_edge[2]:
                max_edge = edge
        new_e.remove(max_edge)

    G = nx.Graph()
    G.add_nodes_from(dots)
    G.add_weighted_edges_from(new_e)
    weights = nx.get_edge_attributes(G, 'weight')
    pos = nx.spring_layout(G)
    nx.draw(G, pos, with_labels=True)
    nx.draw_networkx_edge_labels(G, pos, nx.get_edge_attributes(G, 'weight'))
    plt.show()