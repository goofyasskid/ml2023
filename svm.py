import pygame
import numpy as np
from sklearn import svm
from sklearn.model_selection import train_test_split

if __name__ == '__main__':

    pygame.init()
    screen = pygame.display.set_mode((800, 600))
    screen.fill(color='#FFFFFF')
    pygame.display.update()

    colors = ["#EC5353", "#AAAAAA", "#21618C"]

    pressed_flags = [False, False, False]

    current_color = ''

    radius = 3
    points = []
    target = []

    linear = ''
    rbf = ''
    accuracy = {'linear': 0, 'rbf': 0}

    new_points = []

    while True:
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                pygame.quit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button in [1, 2, 3]:
                    current_color = colors[event.button - 1]
                    pressed_flags[event.button - 1] = True

            if event.type == pygame.MOUSEBUTTONUP:
                current_color = ''
                pressed_flags = [False, False, False]

            if pressed_flags[0] or pressed_flags[2]:
                coord = event.pos
                pygame.draw.circle(screen, color=current_color,
                                   center=coord, radius=radius)
                points.append([coord[0], coord[1]])
                if pressed_flags[0]:
                    target.append(-1)
                else:
                    target.append(1)

            if pressed_flags[1]:
                coord = event.pos
                pygame.draw.circle(screen, color=current_color,
                                   center=coord, radius=radius)
                new_points.append([coord[0], coord[1]])

            if event.type == pygame.KEYUP:
                if event.key == 32 and new_points and linear and rbf:
                    for point in new_points:
                        if accuracy['linear'] >= accuracy['rbf']:
                            point_predict = linear.predict([[point[0], point[1]]])
                        else:
                            point_predict = rbf.predict([[point[0], point[1]]])
                        if point_predict[0] == [-1]:
                            current_color = colors[0]
                        else:
                            current_color = colors[2]
                        pygame.draw.circle(screen, color=current_color,
                                           center=[point[0], point[1]], radius=radius)
                        points.append([point[0], point[1]])
                        target.append(point_predict[0])

                        new_points = []

                if event.key == 13:
                    screen.fill(color='#FFFFFF')
                    points = []
                    target = []
                    linear = ''
                    rbf = ''
                    new_points = []

                if event.key == 9 and 1 in target and -1 in target:
                    length_points = len(points)
                    x_train, x_test, y_train, y_test = train_test_split(points, target, test_size=0.25,
                                                                        random_state=42)
                    linear = svm.SVC(kernel="linear")
                    linear.fit(points, target)

                    rbf = svm.SVC(kernel='rbf', degree=5, random_state=42)
                    rbf.fit(points, target)

                    error = [0, 0]
                    for i, element in enumerate(x_test):
                        predict_linear = linear.predict([[element[0], element[1]]])
                        predict_rbf = rbf.predict([[element[0], element[1]]])
                        if predict_linear != y_test[i]:
                            error[0] += 1
                        if predict_rbf != y_test[i]:
                            error[1] += 1

                    accuracy['linear'] = ((length_points - error[0]) / length_points) * 100
                    accuracy['rbf'] = ((length_points - error[1]) / length_points) * 100

                    print(accuracy)

                    if accuracy['linear'] >= accuracy['rbf']:
                        w = linear.coef_[0]
                        for i in range(3):
                            width = 1
                            if i == 1:
                                width = 3
                            pygame.draw.line(screen, '#606060',
                                             [0, 0 - (linear.intercept_[0] + (i - 1)) / w[1]],
                                             [800, -(w[0] / w[1]) * 800 - (linear.intercept_[0] + (i - 1)) / w[1]],
                                             width)

                    else:
                        x_min = 0
                        x_max = 800
                        y_min = 0
                        y_max = 600
                        x_range, y_range = np.meshgrid(np.arange(x_min, x_max, 10),
                                                       np.arange(y_min, y_max, 10))
                        rbf_target = rbf.predict(np.c_[x_range.ravel(), y_range.ravel()])
                        rbf_target = rbf_target.reshape(x_range.shape)
                        for i in range(len(x_range)):
                            for j in range(len(x_range[i])):
                                if rbf_target[i][j] == 1:
                                    color = '#F1F1FF'
                                else:
                                    color = '#FFA0A0'
                                pygame.draw.circle(screen, color=color,
                                                   center=[x_range[i][j], y_range[i][j]], radius=1)

            pygame.display.update()
