import math
import random


def panmixia(count):
    choice_ = [x for x in range(count)]
    first_parent = random.choice(choice_)
    choice_.remove(first_parent)
    second_parent = random.choice(choice_)
    return first_parent, second_parent


def inbreeding(count, sums):
    choice_ = [x for x in range(count)]
    first_parent = random.choice(choice_)
    second_parent = 0
    near_parent = abs(sums[first_parent])
    for i in range(len(sums)):
        if i != first_parent:
            difference = abs(sums[first_parent] - sums[i])
            if difference < near_parent:
                near_parent = difference
                second_parent = i
    return first_parent, second_parent


def outbreeding(count, sums):
    choice_ = [x for x in range(count)]
    first_parent = random.choice(choice_)
    second_parent = 0
    near_parent = 0
    for i in range(len(sums)):
        if i != first_parent:
            difference = abs(sums[first_parent] - sums[i])
            if difference >= near_parent:
                near_parent = difference
                second_parent = i
    return first_parent, second_parent


class Generations:
    def __init__(self, count):
        self.generation_step = 0
        self.generation_count = count
        self.solutions = []
        self.summa = []
        self.length = 0
        self.children = []

    def add_new_solutions(self):
        for i in range(self.generation_count):
            solution = [random.randint(1, variants[i]) for i in range(n)]
            self.solutions.append(solution)
            summ = 0
            for i in range(n):
                summ += coefficienthints[i] * solution[i]
            self.summa.append(summ)
            self.length += 1

    def find_summs(self):
        for j in range(self.generation_count):
            summ = 0
            for i in range(n):
                summ += coefficienthints[i] * self.solutions[j][i]
            self.summa.append(summ)
            self.length += 1

    def create_child(self, first_p, second_p):
        first_d = abs(answer - self.summa[first_p])
        second_d = abs(answer - self.summa[second_p])
        if first_d < second_d:
            first_gen_count = random.randint(math.ceil(n/2), n-1)
            first_gen = [self.solutions[first_p][0:first_gen_count],
                         self.solutions[first_p][(n - first_gen_count):n]]
        else:
            first_gen_count = random.randint(1, n//2)
            first_gen = [self.solutions[first_p][0:first_gen_count],
                         self.solutions[first_p][(n - first_gen_count):n]]
        second_gen = [self.solutions[second_p][first_gen_count:n],
                      self.solutions[second_p][0:(n - first_gen_count)]]
        child = [first_gen[0] + second_gen[0], first_gen[1] + second_gen[1]]
        self.children.extend(child)

    def next_generation(self, count):
        self.generation_step += 1
        self.generation_count = count
        self.solutions = self.children
        self.summa = []
        self.length = 0
        self.children = []
        self.find_summs()

    def remove_bad_solution(self):
        diff = [abs(answer - self.summa[i]) for i in range(self.generation_count)]
        all_dist = sum(diff)
        remove_change = random.randint(1, all_dist)
        step = 0
        for i in range(self.generation_count):
            step += diff[i]
            if remove_change <= step:
                self.solutions.pop(i)
                self.length -= 1
                self.generation_count -= 1
                self.summa.pop(i)
                break


if __name__ == '__main__':
    random.seed()
    n = random.randint(5, 10)
    count_variant = random.randint(3, 10)
    coefficienthints = [random.randint(1, 20) for _ in range(n)]
    answer = random.randint(n*20, n**2 * 20)
    variants = [answer//coefficienthints[i] for i in range(n)]

    print(f"{coefficienthints} = {answer}")

    generation = Generations(count_variant)
    generation.add_new_solutions()

    for _ in range(random.randint(50, 200)):
        for i in range(random.randint(20, 50)):
            select = random.randint(1, 3)
            if select == 3:
                first_parent, second_parent = panmixia(generation.length)
                generation.create_child(first_parent, second_parent)
            elif select == 2:
                first_parent, second_parent = inbreeding(generation.length, generation.summa)
                generation.create_child(first_parent, second_parent)
            elif select == 1:
                first_parent, second_parent = outbreeding(generation.length, generation.summa)
                generation.create_child(first_parent, second_parent)

        generation.next_generation(len(generation.children))
        while generation.generation_count > 20:
            generation.remove_bad_solution()

        for i in range(generation.generation_count):
            if generation.summa[i] == answer:
                print(f"Эти коэффециенты дают правильное решение - {generation.solutions[i]}. Для уравнения {coefficienthints} = {answer}")
                exit()


    near = abs(answer - generation.summa[0])
    for i in range(generation.generation_count):
        difference = abs(answer - generation.summa[i])
        if difference <= near:
            near = difference
            result = i

    print(f"Ближайшие коэффециенты - {generation.solutions[result]}, с ответом - {generation.summa[result]}. Для уравнения {coefficienthints} = {answer}")
