import random
import matplotlib.pyplot as plt
import numpy as np


class Point:
    def __init__(self, x_, y_, color=(0.5, 0.5, 0.5, 0.5)):
        self.x = x_
        self.y = y_
        self.color = color


class Centroid:
    def __init__(self, x_, y_, color):
        self.x = x_
        self.y = y_
        self.color = color
        self.points = []


class Scheme:
    def __init__(self):
        self.points = []
        self.centroids = []
        self.center = None
        self.radius = 0
        self.amount_of_cluster_scheme = []

    def get_points(self):
        with open('points.txt') as f:
            for line in f:
                line = line.split(',')
                self.points.append(Point(int(line[0]), int(line[1])))
        self.count_center()
        self.count_radius()

    def count_center(self):
        self.center = Point(sum([point.x for point in self.points]) / len(self.points),
                            sum([point.y for point in self.points]) / len(self.points))

    def count_radius(self):
        for point in self.points:
            cur_dist = dist(point, self.center)
            if cur_dist > self.radius:
                self.radius = cur_dist

    def count_centroids_amount(self):
        previous_point = Point(0, 0)
        min_distance = None
        min_point = None
        previous_distance = 0
        for i in range(1, 31):
            self.centroids = []
            self.create_circle(i)
            over = False
            while over is False:
                self.rearrange_points()
                over = self.recount_centroids()

            current_point = Point(i, self.count_centroid_y())
            self.amount_of_cluster_scheme.append(current_point)
            if i == 1:
                pass
            elif i == 2:
                previous_distance = dist(previous_point, current_point)
            elif i == 3:
                current_distance = dist(previous_point, current_point)
                min_distance = current_distance / previous_distance
                min_point = i - 1
            else:
                current_distance = dist(previous_point, current_point)
                distance = current_distance / previous_distance
                if distance < min_distance:
                    min_distance = distance
                    min_point = i - 1

            previous_point = current_point

        print(min_point)
        self.show_scheme()
        self.centroids = []
        self.create_circle(min_point)
        over = False
        while over is False:
            self.rearrange_points()
            self.draw_points()
            over = self.recount_centroids()
        print("final")
        self.draw_points()

    def create_circle(self, points_amount):
        for centroid in range(points_amount):
            color = (random.uniform(0.0, 1.0),
                     random.uniform(0.0, 1.0),
                     random.uniform(0.0, 1.0), 1.0)
            self.centroids.append(Centroid(self.radius * np.cos(2 * np.pi * centroid / points_amount) + self.center.x,
                                           self.radius * np.sin(2 * np.pi * centroid / points_amount) + self.center.y,
                                           color))

    def rearrange_points(self):
        for centroid in self.centroids:
            centroid.points = []
        for point in self.points:
            min_distance = dist(point, self.centroids[0]) + 1
            min_centroid = None
            for centroid in self.centroids:
                distance = dist(point, centroid)
                if distance < min_distance:
                    min_distance = distance
                    min_centroid = centroid
            point.color = min_centroid.color
            min_centroid.points.append(point)

    def recount_centroids(self):
        changed = True
        for centroid in self.centroids:
            try:
                new_x = round(sum([point.x for point in centroid.points]) / len(centroid.points), 3)
                new_y = round(sum([point.y for point in centroid.points]) / len(centroid.points), 3)
            except:
                new_x = centroid.x
                new_y = centroid.y
            if centroid.x != new_x or centroid.y != new_y:
                changed = False
            centroid.x = new_x
            centroid.y = new_y
        return changed

    def count_centroid_y(self):
        sum = 0
        for centroid in self.centroids:
            for point in centroid.points:
                sum += dist(centroid, point) ** 2
        return sum

    def draw_points(self):
        for point in self.points:
            plt.scatter(int(point.x), int(point.y), color=point.color, s=25)
        for centroid in self.centroids:
            plt.scatter(int(centroid.x), int(centroid.y), color=centroid.color, s=75)
        plt.show()

    def show_scheme(self):
        plt.plot([point.x for point in self.amount_of_cluster_scheme],
                 [point.y for point in self.amount_of_cluster_scheme])
        plt.show()


def random_points():
    f = open('points.txt', 'w')
    for i in range(100):
        f.write(f"{random.randint(0, 99)},{random.randint(0, 99)}\n")


def dist(point_a, point_b):
    return np.sqrt((point_a.x - point_b.x) ** 2 + (point_a.y - point_b.y) ** 2)


if __name__ == '__main__':
    #random_points()
    scheme = Scheme()
    scheme.get_points()
    scheme.count_centroids_amount()
