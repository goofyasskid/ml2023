import cv2
from simple_facerec import FaceRecognition


def webcam_loop():
    ret, frame = cap.read()

    locations, names = sfr.detect_faces(frame)
    for location, name in zip(locations, names):
        y1, x2, y2, x1 = location[0], location[1], location[2], location[3]

        cv2.putText(frame, name, (x1, y1 - 10), cv2.FONT_HERSHEY_TRIPLEX, 1, (0, 128, 0), 2)
        cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 200, 0), 4)

    cv2.imshow("Frame", frame)


if __name__ == '__main__':

    sfr = FaceRecognition()
    sfr.load_images("images/")

    cap = cv2.VideoCapture(0)

    while True:
        webcam_loop()

        input_key = cv2.waitKey(1)
        if input_key == 27:
            cap.release()
            cv2.destroyAllWindows()
