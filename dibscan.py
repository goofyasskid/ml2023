import pygame
import numpy as np
import random


class Point:
    def __init__(self, x_, y_, color=None, cluster="#000000"):
        self.x = x_
        self.y = y_
        self.color = color
        self.neighbours = []
        self.cluster = cluster


class DbscanScheme:
    def __init__(self, range_, green):
        self.points = []
        self.questionable_points = []
        self.max_range = range_
        self.neighbours_to_green = green

    def calculate(self):
        for point in self.points:
            point.color = None
        for point in self.points:
            if not point.color:
                self.find_point_neighbours(point, random_color())

        for point in self.questionable_points:
            range_ = self.max_range + 1
            for neighbour in point.neighbours:
                if neighbour.color == "#008000" and dist(point, neighbour) < range_:
                    range_ = dist(point, neighbour)
                    point.color = "#FFFF00"
                    point.cluster = neighbour.cluster

    def find_point_neighbours(self, main_point, cluster_color):
        for point in self.points:
            if dist(main_point, point) <= self.max_range and point not in main_point.neighbours:
                main_point.neighbours.append(point)
        if len(main_point.neighbours) >= self.neighbours_to_green:
            main_point.color = "#008000"
            main_point.cluster = cluster_color
            for point in main_point.neighbours:
                if not point.color:
                    self.find_point_neighbours(point, cluster_color)
        else:
            main_point.color = "#FF0000"
            self.questionable_points.append(main_point)


def random_color():
    r = lambda: random.randint(0, 255)
    color = ('#%02X%02X%02X' % (r(), r(), r()))
    return color


def dist(point_a, point_b):
    return np.sqrt((point_a.x-point_b.x)**2+(point_a.y-point_b.y)**2)


def generate_random_points(point):
    count = random.randint(2, 5)
    points = []
    for i in range(count):
        angle = np.pi*random.randint(0, 360)/180
        radius = random.randint(10, 20)
        x = radius*np.cos(angle)+point.x
        y = radius*np.sin(angle)+point.y
        points.append(Point(x, y))
    return points


if __name__ == '__main__':
    r = 3
    pygame.init()
    screen = pygame.display.set_mode((600, 400), pygame.RESIZABLE)
    screen.fill(color='#FFFFFF')
    pygame.display.update()
    is_pressed = False
    scheme = DbscanScheme(15, 3)
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == pygame.VIDEORESIZE:
                screen.fill(color='#FFFFFF')
                for point in scheme.points:
                    pygame.draw.circle(screen, color='#FF0000', center=(point.x, point.y), radius=r)
            if event.type == pygame.KEYUP:
                if event.key == 13:
                    screen.fill(color='#FFFFFF')
                    scheme.calculate()
                    for point in scheme.points:
                        pygame.draw.circle(screen, color=point.color, center=(point.x, point.y), radius=r)
                if event.key == 1073742053:
                    screen.fill(color='#FFFFFF')
                    scheme.calculate()
                    for point in scheme.points:
                        pygame.draw.circle(screen, color=point.cluster, center=(point.x, point.y), radius=r)
                if event.key == 27:
                    screen.fill(color='#FFFFFF')
                    scheme.points = []

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    is_pressed = True
            if event.type == pygame.MOUSEBUTTONUP:
                is_pressed = False
            if is_pressed:
                coord = event.pos
                coord = Point(coord[0], coord[1])
                if len(scheme.points):
                    if dist(scheme.points[-1], coord) > 5*r:
                        pygame.draw.circle(screen, color='#FF0000', center=(coord.x, coord.y), radius=r)
                        near_point = generate_random_points(coord)
                        scheme.points.extend(near_point)
                        for elem in near_point:
                            pygame.draw.circle(screen, color='#FF0000', center=(elem.x, elem.y), radius=r)
                        scheme.points.append(coord)
                else:
                    pygame.draw.circle(screen, color='#FF0000', center=(coord.x, coord.y), radius=r)
                    scheme.points.append(coord)

            pygame.display.update()
