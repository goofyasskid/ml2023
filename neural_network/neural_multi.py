import pygame
import numpy as np
import random
from PIL import Image, ImageDraw
from keras.models import load_model
import os


class Point:
    def __init__(self, x_, y_, color=None, cluster="#000000"):
        self.x = x_
        self.y = y_
        self.color = color
        self.neighbours = []
        self.cluster = cluster


class DbscanScheme:
    def __init__(self, range_, green):
        self.points = []
        self.questionable_points = []
        self.max_range = range_
        self.neighbours_to_green = green
        self.clusters = {}
        self.images_names = []
        self.order = {}
        self.canvases = {}

    def calculate(self):
        for point in self.points:
            point.color = None
        for point in self.points:
            if not point.color:
                color = random_color()
                self.clusters[color] = []
                self.find_point_neighbours(point, color)

        for point in self.questionable_points:
            range_ = self.max_range + 1
            for neighbour in point.neighbours:
                if neighbour.color == "#008000" and dist(point, neighbour) < range_:
                    range_ = dist(point, neighbour)
                    point.color = "#FFFF00"
                    point.cluster = neighbour.cluster

    def find_point_neighbours(self, main_point, cluster_color):
        for point in self.points:
            if dist(main_point, point) <= self.max_range and point not in main_point.neighbours:
                main_point.neighbours.append(point)
        if len(main_point.neighbours) >= self.neighbours_to_green:
            main_point.color = "#008000"
            main_point.cluster = cluster_color
            for point in main_point.neighbours:
                if not point.color:
                    self.find_point_neighbours(point, cluster_color)
        else:
            main_point.color = "#FF0000"
            self.questionable_points.append(main_point)

    def find_canvas_class(self, img_url, radius, size_y, size_x):
        for point in self.points:
            if point.cluster != "#000000":
                self.clusters[point.cluster].append(point)

        canvases = []
        im = Image.open(img_url)
        for cluster in self.clusters.values():
            if cluster:
                max_x = max(cluster, key=lambda x: x.x).x
                min_x = min(cluster, key=lambda x: x.x).x
                max_y = max(cluster, key=lambda x: x.y).y
                min_y = min(cluster, key=lambda x: x.y).y

                canvases.append({'max_x': max_x, 'min_x': min_x, 'max_y': max_y, 'min_y': min_y})
                self.canvases[min_x] = [max_x - min_x, max_y - min_y]


        for index, coordinate in enumerate(canvases):
            width = coordinate['max_y'] - coordinate['min_y']
            height = coordinate['max_x'] - coordinate['min_x']
            difference = height - width

            if difference <= 0:
                added = width * 0.5 // 2
                min_y = coordinate['min_y'] - added
                max_y = coordinate['max_y'] + added
                min_x = coordinate['min_x'] - ((difference * (-1)) // 2 + added)
                max_x = coordinate['max_x'] + ((difference * (-1)) // 2 + added)
            else:
                added = height * 0.5 // 2
                min_y = coordinate['min_y'] - (difference // 2 + added)
                max_y = coordinate['max_y'] + (difference // 2 + added)
                min_x = coordinate['min_x'] - added
                max_x = coordinate['max_x'] + added

            im_crop_outside = im.crop((min_x, min_y, max_x, max_y))
            im_crop_outside.save(f'assets/crop.jpg', quality=95)
            po = Image.open('assets/crop.jpg')
            draw = ImageDraw.Draw(po)

            for i in canvases:
                if i != coordinate:
                    if ((min_x <= i['min_x'] <= max_x and
                         (min_y <= i['max_y'] <= max_y or min_y <= i['min_y'] <= max_y))
                            or (min_x <= i['max_x'] <= max_x and
                                (min_y <= i['max_y'] <= max_y or min_y <= i['min_y'] <= max_y))):
                        draw.rectangle((i['min_x'] - min_x - radius,
                                        i['min_y'] - min_y - radius,
                                        i['max_x'] - min_x + radius,
                                        i['max_y'] - min_y + radius), 'black', 'black')
            po.save(f'digits/{index}.jpg', quality=95)
            self.order[(coordinate['min_x'])] = f'digits/{index}.jpg'
            self.images_names.append(f'digits/{index}.jpg')


def random_color():
    r = lambda: random.randint(0, 255)
    color = ('#%02X%02X%02X' % (r(), r(), r()))
    return color


def dist(point_a, point_b):
    return np.sqrt((point_a.x-point_b.x)**2+(point_a.y-point_b.y)**2)


def generate_random_points(point):
    count = random.randint(2, 5)
    points = []
    for i in range(count):
        angle = np.pi*random.randint(0, 360)/180
        radius = random.randint(10, 20)
        x = radius*np.cos(angle)+point.x
        y = radius*np.sin(angle)+point.y
        points.append(Point(x, y))
    return points


def capture(display, name, pos, size):
    image = pygame.Surface(size)
    image.blit(display, (0, 0), (pos, size))
    pygame.image.save(image, name)


def recognize_digit(images, order):
    number = ''
    model = load_model('keras_e/test_mnist.keras')

    for i in order:
        img = image_processing(images[i])
        num = np.argmax(model.predict(np.array([img]))[0])

        img = image_processing(images[i], True)
        print(num)
        if np.argmax(model.predict(np.array([img]))[0]) == 1:
            num = "-"

        print(num)

        number += str(num)
    return number


def image_processing(image_url, rotation=False):
    img = Image.open(image_url)
    if rotation:
        img = img.rotate(90)
    img = img.resize((28, 28))
    img = img.convert("L")
    img.save('assets/_.jpg')
    img = np.asarray(img)

    img = img.reshape(28, 28, 1)
    img = img / 255.0
    return img


if __name__ == '__main__':
    r = 9
    point_dist = 1
    pygame.init()
    size_x, size_y = 600, 400
    screen = pygame.display.set_mode((size_x, size_y), pygame.RESIZABLE)
    screen.fill(color='#000000')
    pygame.display.update()
    is_pressed = False
    scheme = DbscanScheme(15, 3)
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == pygame.VIDEORESIZE:
                screen.fill(color='#000000')
                for point in scheme.points:
                    pygame.draw.circle(screen, color='#FFFFFF', center=(point.x, point.y), radius=r)
            if event.type == pygame.KEYUP:
                if event.key == 1073742053:
                    screen.fill(color='#000000')
                    scheme.calculate()
                    for point in scheme.points:
                        pygame.draw.circle(screen, color='#FFFFFF', center=(point.x, point.y), radius=r)
                    capture(screen, "assets/capture.png", (0, 0), (size_x, size_y))
                    scheme.find_canvas_class("assets/capture.png", r, size_y, size_x)


                    order_key = sorted(scheme.order, key=lambda x: x)

                    im_index = order_key.pop(0)

                    while len(order_key) != 0:

                    print(recognize_digit(scheme.order, order_key))
                if event.key == 27:
                    screen.fill(color='#000000')
                    scheme.points = []
                    scheme.images_names = []
                    scheme.order = {}
                    scheme.clusters = {}
                    scheme.canvases = {}

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    is_pressed = True
            if event.type == pygame.MOUSEBUTTONUP:
                is_pressed = False
            if is_pressed:
                coord = event.pos
                coord = Point(coord[0], coord[1])
                if len(scheme.points):
                    if dist(scheme.points[-1], coord) > 5*point_dist:
                        pygame.draw.circle(screen, color='#FFFFFF', center=(coord.x, coord.y), radius=r)
                        near_point = generate_random_points(coord)
                        scheme.points.append(coord)
                else:
                    pygame.draw.circle(screen, color='#FFFFFF', center=(coord.x, coord.y), radius=r)
                    scheme.points.append(coord)

            pygame.display.update()
